import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'hem',
    pathMatch: 'full'
  },
  {
    path: 'hem',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'produkter',
    loadChildren: () => import('./pages/produkter/produkter.module').then( m => m.ProdukterPageModule)
  },
  {
    path: 'nyheter',
    loadChildren: () => import('./pages/nyheter/nyheter.module').then( m => m.NyheterPageModule)
  },
  {
    path: 'om',
    loadChildren: () => import('./pages/om/om.module').then( m => m.OmPageModule)
  },
  {
    path: 'kylen',
    loadChildren: () => import('./pages/kylen/kylen.module').then( m => m.KylenPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
