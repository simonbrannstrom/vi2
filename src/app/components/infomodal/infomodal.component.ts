import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ContactComponent } from '../contact/contact.component';

@Component({
  selector: 'app-infomodal',
  templateUrl: './infomodal.component.html',
  styleUrls: ['./infomodal.component.scss'],
})
export class InfomodalComponent implements OnInit {

  modal: HTMLIonModalElement;
  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() { }

  async closeAndContact() {
    this.modal = await this.modalCtrl.getTop();
    this.modal.dismiss();
    const modal = await this.modalCtrl.create({
      component: ContactComponent,
      cssClass: 'auto-height',
      swipeToClose: true,
    });
    this.modal = await this.modalCtrl.getTop();
    await modal.present();
    await modal.onWillDismiss().then(() => {
      delete this.modal;
    });
  }

  async dismiss() {
    await this.modalCtrl.dismiss();
  }
}
