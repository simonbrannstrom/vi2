import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { IProduceType } from 'src/app/common/interfaces';

@Component({
  selector: 'app-producemodal',
  templateUrl: './producemodal.component.html',
  styleUrls: ['./producemodal.component.scss'],
})
export class ProducemodalComponent implements OnInit {

  @Input() produce: IProduceType;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() { }

  async dismiss() {
    await this.modalCtrl.dismiss();
  }
}
