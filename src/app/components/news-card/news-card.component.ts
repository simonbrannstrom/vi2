import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { ICard, ICardType, IWPPosts } from 'src/app/common/interfaces';
import { ApiService } from 'src/app/services/api.service';
import { ContactComponent } from '../contact/contact.component';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss'],
})
export class NewsCardComponent implements OnInit {

  @Input() type: string;

  julbord: string[];
  content: string[];
  title = '';
  description: string;
  cards: ICard[] = [] as ICard[];
  cardType = ICardType;
  modal: HTMLIonModalElement;
  constructor(private modalCtrl: ModalController) {
  }

  ngOnInit() {
    // JULBORD
    this.cards.push({
      type: ICardType.JULBORD,
      img: '/assets/stock/christmas.png',
      title: 'Julbord 339:-',
      content: [
        'Svensk griljerad julskinka',
        'Skånsk senap',
        'Gravad lax',
        'Hovmästarsås',
        'Halstrad laxsida med skagenröra',
        'Ägghalvor med handskalade räkor',
        'Norrbottnisk renkorv',
        'Inlagd sill, sikromssill',
        'Julost',
        'Gubbröra',
        'Rödbetssallad',
        'Leverpastej och inlagd gurka',
        'Nybakad jullimpa och smör'
      ],
      extraTitle: 'Småvarmt',
      extraContent: [
        'Jansons frestelse',
        'Riktiga köttbullar',
        'Grillade marinerade revbensspjäll',
        'Prinskorv',
        'Kokt potatis'
      ],
    });

    // JULTALLRIK
    this.cards.push({
      type: ICardType.JULTALLRIK,
      img: '/assets/stock/ham.png',
      title: 'Jultallrik 189:- (med Småvarmt 269:-)',
      content: [
        'Svensk griljerad julskinka',
        'Skånsk senap',
        'Gravad lax',
        'Hovmästarsås',
        'Ägghalvor med handskalade räkor',
        'Norrbottnisk renkorv',
        'Inlagd sill, sikromssill',
        'Julost',
        'Gubbröra',
        'Rödbetssallad',
        'Leverpastej och inlagd gurka',
        'Nybakad jullimpa och smör'
      ],
      extraTitle: 'Småvarmt',
      extraContent: [
        'Jansons frestelse',
        'Riktiga köttbullar',
        'Grillade marinerade revbensspjäll',
        'Prinskorv',
        'Kokt potatis'
      ],
    });

    this.cards.push({
      type: ICardType.RISGRYNSGROT,
      img: '/assets/stock/christmas.png',
      title: 'Vår egen kokta risgrynsgröt 129:-',
      content: [
        'Mjölk',
        'Kanel',
        'Socker',
        'Svensk grilljerad julskinka med senap, nybakat Julbröd med ost & smör'
      ]
    });
    //SMÅVARMT
    // this.cards.push({
    //   type: ICardType.SMAVARMT,
    //   img: '/assets/stock/christmas.jpeg',
    //   title: 'Småvarmt',
    //   content: [
    //     'Jansons frestelse',
    //     'Riktiga köttbullar',
    //     'Grillade marinerade revbensspjäll',
    //     'Prinskorv',
    //     'Kokt potatis'
    //   ]
    // });
    // JULDESSERT
    this.cards.push({
      img: '/assets/stock/juldessert.jpeg',
      type: ICardType.JULDESSERT,
      title: 'Juldessert 79:- / pers',
      content: [
        'Pepparkakspannacotta med hjorton & knäckeflarn',
        'Lyxig chokladtårta med karamell, flingsalt & lättvispad grädde',
        'Ris a la malta (minst 10pers)'
      ]
    });
  }

  async contact() {
    if (this.modal) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ContactComponent,
      cssClass: 'auto-height',
      swipeToClose: true,
    });
    this.modal = await this.modalCtrl.getTop();
    await modal.present();
    await modal.onWillDismiss().then(() => {
      delete this.modal;
    });
  }
}
