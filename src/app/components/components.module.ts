import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HeroComponent } from './hero/hero.component';
import { InfomodalComponent } from './infomodal/infomodal.component';
import { NewsCardComponent } from './news-card/news-card.component';
import { ProducemodalComponent } from './producemodal/producemodal.component';

@NgModule({
  declarations: [
    HeaderComponent,
    NewsCardComponent,
    FooterComponent,
    HeroComponent,
    ProducemodalComponent,
    InfomodalComponent
    ],
  imports: [
    RouterModule,
    IonicModule,
    CommonModule,
  ],
  exports: [
    HeaderComponent,
    NewsCardComponent,
    FooterComponent,
    HeroComponent,
    ProducemodalComponent,
    InfomodalComponent
  ],
  providers: [

  ],
})
export class ComponentsModule {}
