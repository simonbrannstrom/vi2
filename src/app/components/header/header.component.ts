import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { filter } from 'rxjs/operators';
import { IPage } from 'src/app/common/interfaces';
import { ContactComponent } from '../contact/contact.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  pages: IPage[] = [
    {
      name: 'Hem',
      link: '/home',
      icon: 'home',
    },
    {
      name: 'Ingredienser',
      link: '/ingredients',
      icon: 'bag'
    },
    {
      name: 'Maträtter',
      link: '/dishes',
      icon: 'restaurant'
    },
  ];
  breadcrumb = '';
  modal: HTMLIonModalElement;

  constructor(private router: Router, private modalCtrl: ModalController) {
    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd)
      )
      .subscribe((navEnd: NavigationEnd) => {
        switch (navEnd.urlAfterRedirects) {
          case '/home':
            this.breadcrumb = 'Hem';
            break;
          case '/ingredients':
            this.breadcrumb = 'Ingredienser';
            break;
          case '/dishes':
            this.breadcrumb = 'Maträtter';
            break;
          case '/recipes':
            this.breadcrumb = 'Mina Recept';
            break;
          case '/waste':
            this.breadcrumb = 'Svinn';
            break;
          default:
            break;
        }
      });
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: ContactComponent,
      cssClass: 'auto-height'
    });
    this.modal = await this.modalCtrl.getTop();

    await modal.present();
    await modal.onWillDismiss().then(() => {
      delete this.modal;
    });
  }

  ngOnInit() {
  }

}
