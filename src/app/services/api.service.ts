/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/dot-notation */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IWPPosts } from '../common/interfaces';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiUrl = environment.apiUrl;
  totalPosts = null;
  pages: IWPPosts[] = [] as IWPPosts[];

  constructor(private http: HttpClient) { }

  getPosts(baseUrl?: string, page = 1): Observable<IWPPosts[]> {
    const options = {
      observe: 'response' as 'body',
      params: {
        per_page: '5',
        page: '' + page
      }
    };
    const wpUrl = baseUrl ? baseUrl + '/wp-json/wp/v2/' : this.apiUrl;
    return this.http.get<any[]>(`${wpUrl}posts?_embed`, options).pipe(
      map((resp: any) => {
        this.pages = resp['headers'].get('x-wp-totalpages');
        this.totalPosts = resp['headers'].get('x-wp-total');

        const data: IWPPosts[] = resp['body'];

        for (const post of data) {
          post.media_url = post._embedded['wp:featuredmedia'] ? post._embedded['wp:featuredmedia'][0]?.source_url : './assets/logo.png';
        }
        console.log(data);
        return data;
      })
    );
  }

  getPages(baseUrl?: string, page = 1): Observable<IWPPosts[]> {
    const options = {
      observe: 'response' as 'body',
      params: {
        per_page: '5',
        page: '' + page
      }
    };
    const wpUrl = baseUrl ? baseUrl + '/wp-json/wp/v2/' : this.apiUrl;
    return this.http.get<any[]>(`${wpUrl}pages?_embed`, options).pipe(
      map((resp: any) => {
        this.pages = resp['headers'].get('x-wp-totalpages');
        this.totalPosts = resp['headers'].get('x-wp-total');

        const data: IWPPosts[] = resp['body'];

        for (const post of data) {
          post.media_url = post._embedded['wp:featuredmedia'] ? post._embedded['wp:featuredmedia'][0]?.source_url : './assets/logo.png';
        }
        return data;
      })
    );
  }

  getPostContent(id: number) {
    return this.http.get(`${this.apiUrl}posts/${id}?_embed`).pipe(
      map((post: IWPPosts) => {
        post['media_url'] = post['_embedded']['wp:featuredmedia'][0]?.source_url;
        return post;
      })
    );
  }
}
