import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NyheterPage } from './nyheter.page';

const routes: Routes = [
  {
    path: '',
    component: NyheterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NyheterPageRoutingModule {}
