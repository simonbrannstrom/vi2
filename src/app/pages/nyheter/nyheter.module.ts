import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NyheterPageRoutingModule } from './nyheter-routing.module';

import { NyheterPage } from './nyheter.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    NyheterPageRoutingModule
  ],
  declarations: [NyheterPage]
})
export class NyheterPageModule {}
