import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, IonSlides, ModalController, Platform } from '@ionic/angular';
import { IProduceType } from 'src/app/common/interfaces';
import { ContactComponent } from 'src/app/components/contact/contact.component';
import { InfomodalComponent } from 'src/app/components/infomodal/infomodal.component';
import { ProducemodalComponent } from 'src/app/components/producemodal/producemodal.component';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('slides') slides: IonSlides;
  @ViewChild('content') private content: IonContent;
  slideIndex = 0;
  slideOpts = {
    initialSlide: 1.15,
    autoplay: false,
  };
  isMobile = false;
  cards = [
    {
      logo: 'palt.jpeg',
      title: 'Husmanskost',
      subtitle: 'Husmanskost / Lunch',
      link: IProduceType.HUSMANSKOST
    },
    {
      logo: 'smaratter.png',
      title: 'Smårätter',
      subtitle: 'Smårätter / Förrätter',
      link: IProduceType.SMARATTER
    },
    {
      logo: 'varmratter.png',
      title: 'Varmrätter',
      subtitle: '',
      link: IProduceType.VARMRATTER
    },
    {
      logo: 'hjortron.png',
      title: 'Desserter',
      subtitle: '',
      link: IProduceType.EFTERRATTER
    },
    {
      logo: 'buffeer.png',
      title: 'Bufféer',
      subtitle: '',
      link: IProduceType.BUFFEER
    },
    {
      logo: 'kallaratter.png',
      title: 'Kalla rätter',
      subtitle: 'Sallader/Pajer/Kalla rätter',
      link: IProduceType.KALLARATTER
    },
  ];
  modal: HTMLIonModalElement;

  constructor(
    private api: ApiService,
    private modalCtrl: ModalController,
    private platform: Platform
  ) {
    this.isMobile = this.platform.is('desktop') ? false : true;
  }

  ngOnInit() {
    // this.getPages();
    // this.infoModal();
    // this.produceModal(IProduceType.HUSMANSKOST);
  }

  async contact() {
    if (this.modal) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ContactComponent,
      cssClass: 'auto-height',
      swipeToClose: true,
    });
    this.modal = await this.modalCtrl.getTop();
    await modal.present();
    await modal.onWillDismiss().then(() => {
      delete this.modal;
    });
  }

  scrollToLabel(label) {
    const titleEl = document.getElementById(label);
    this.content.scrollToPoint(0, titleEl.offsetTop, 700);
  }

  async produceModal(type: IProduceType) {
    if (this.modal) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ProducemodalComponent,
      cssClass: 'produceModal',
      swipeToClose: true,
      componentProps: [
        type
      ]
    });
    this.modal = await this.modalCtrl.getTop();
    await modal.present();
    await modal.onWillDismiss().then(() => {
      delete this.modal;
    });
  }


  getPages() {
    this.api.getPages().subscribe(res => {
      console.log(res);
    });
  }

  async infoModal() {
    if (this.modal) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: InfomodalComponent,
      cssClass: 'auto-height',
      swipeToClose: true,
    });
    this.modal = await this.modalCtrl.getTop();
    await modal.present();
    await modal.onWillDismiss().then(() => {
      delete this.modal;
    });
  }
}
