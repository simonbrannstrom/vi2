import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ContactComponent } from 'src/app/components/contact/contact.component';

@Component({
  selector: 'app-kylen',
  templateUrl: './kylen.page.html',
  styleUrls: ['./kylen.page.scss'],
})
export class KylenPage implements OnInit {

  modal: HTMLIonModalElement;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  async contact() {
    if (this.modal) {
      return;
    }
    const modal = await this.modalCtrl.create({
      component: ContactComponent,
      cssClass: 'auto-height',
      swipeToClose: true,
    });
    this.modal = await this.modalCtrl.getTop();
    await modal.present();
    await modal.onWillDismiss().then(() => {
      delete this.modal;
    });
  }
}
