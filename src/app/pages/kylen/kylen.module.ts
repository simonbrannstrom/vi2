import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KylenPageRoutingModule } from './kylen-routing.module';

import { KylenPage } from './kylen.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    KylenPageRoutingModule
  ],
  declarations: [KylenPage]
})
export class KylenPageModule {}
