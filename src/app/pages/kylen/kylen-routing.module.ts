import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KylenPage } from './kylen.page';

const routes: Routes = [
  {
    path: '',
    component: KylenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KylenPageRoutingModule {}
