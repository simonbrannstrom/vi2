import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OmPageRoutingModule } from './om-routing.module';

import { OmPage } from './om.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    OmPageRoutingModule
  ],
  declarations: [OmPage]
})
export class OmPageModule {}
