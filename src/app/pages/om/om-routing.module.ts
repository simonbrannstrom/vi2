import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OmPage } from './om.page';

const routes: Routes = [
  {
    path: '',
    component: OmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OmPageRoutingModule {}
