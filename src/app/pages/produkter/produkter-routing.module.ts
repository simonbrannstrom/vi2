import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProdukterPage } from './produkter.page';

const routes: Routes = [
  {
    path: '',
    component: ProdukterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProdukterPageRoutingModule {}
