import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { IProduceType, IWPPosts } from 'src/app/common/interfaces';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-produkter',
  templateUrl: './produkter.page.html',
  styleUrls: ['./produkter.page.scss'],
})
export class ProdukterPage implements OnInit, OnDestroy {

  sub: Subscription;
  posts: IWPPosts[];
  posts$: Observable<IWPPosts[]>;
  cards = [
    {
      logo: 'palt.jpeg',
      title: 'Husmanskost',
      subtitle: 'Husmanskost / Lunch',
      link: IProduceType.HUSMANSKOST
    },
    {
      logo: 'smaratter.png',
      title: 'Smårätter',
      subtitle: 'Smårätter / Förrätter',
      link: IProduceType.SMARATTER
    },
    {
      logo: 'varmratter.png',
      title: 'Varmrätter',
      subtitle: '',
      link: IProduceType.VARMRATTER
    },
    {
      logo: 'hjortron.png',
      title: 'Desserter',
      subtitle: '',
      link: IProduceType.EFTERRATTER
    },
    {
      logo: 'buffeer.png',
      title: 'Bufféer',
      subtitle: '',
      link: IProduceType.BUFFEER
    },
    {
      logo: 'kallaratter.png',
      title: 'Kalla rätter',
      subtitle: 'Sallader/Pajer/Kalla rätter',
      link: IProduceType.KALLARATTER
    },
  ];
  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getPosts();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getPosts() {
    this.sub = this.api.getPosts().subscribe(res => {
      this.posts = res;
      this.posts = this.posts.slice(0, 3);
      console.log(this.posts);
    });
  }

}
