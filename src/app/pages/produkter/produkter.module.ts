import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProdukterPageRoutingModule } from './produkter-routing.module';

import { ProdukterPage } from './produkter.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ProdukterPageRoutingModule
  ],
  declarations: [ProdukterPage]
})
export class ProdukterPageModule {}
