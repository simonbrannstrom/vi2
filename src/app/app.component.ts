import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { IPage } from './common/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  showProfileMenu = false;
  showFeed = false;
  showSidebar = true;
  showMobileMenu = false;
  pages: IPage[] = [
    {
      name: 'Hem',
      link: '/home',
      icon: 'home',
    },
    {
      name: 'Ingredienser',
      link: '/ingredients',
      icon: 'bag'
    },
    {
      name: 'Maträtter',
      link: '/dishes',
      icon: 'restaurant'
    },
    {
      name: 'Mina Recept',
      link: '/recipes',
      icon: 'receipt'
    },
    {
      name: 'Svinn',
      link: '/waste',
      icon: 'nutrition'
    },
  ];
  breadcrumb: string[] = [];
  data: Observable<any>;
  constructor(private router: Router) {
    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd)
      )
      .subscribe((navEnd: NavigationEnd) => {
        switch (navEnd.urlAfterRedirects) {
          case '/home':
            this.breadcrumb[0] = 'Hem';
            break;
          case '/ingredients':
            this.breadcrumb[0] = 'Ingredienser';
            break;
          case '/dishes':
            this.breadcrumb[0] = 'Maträtter';
            break;
          case '/recipes':
            this.breadcrumb[0] = 'Mina Recept';
            break;
          case '/waste':
            this.breadcrumb[0] = 'Svinn';
            break;
          default:
            break;
        }
      });
  }
}
