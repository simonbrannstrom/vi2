const colors = require('tailwindcss/colors')
module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.{html,ts}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      primary: {
        default: 'var(--ion-color-primary)',
        shade: 'var(--ion-color-primary-shade)',
        tint: 'var(--ion-color-primary-tint)',
      },
      secondary: {
        default: 'var(--ion-color-secondary)',
        shade: 'var(--ion-color-secondary-shade)',
        tint: 'var(--ion-color-secondary-tint)',
      },
      tertiary: {
        default: 'var(--ion-color-tertiary)',
        shade: 'var(--ion-color-tertiary-shade)',
        tint: 'var(--ion-color-tertiary-tint)',
      },
      ionblue: {
        default: 'var(--ion-color-blue)',
        shade: 'var(--ion-color-blue-shade)',
        tint: 'var(--ion-color-blue-tint)',
      },
      light: {
        default: 'var(--ion-color-light)',
        shade: 'var(--ion-color-light-shade)',
        tint: 'var(--ion-color-light-tint)',
      },
      medium: {
        default: 'var(--ion-color-medium)',
        shade: 'var(--ion-color-medium-shade)',
        tint: 'var(--ion-color-medium-tint)',
      },
      dark: {
        default: 'var(--ion-color-dark)',
        shade: 'var(--ion-color-dark-shade)',
        tint: 'var(--ion-color-dark-tint)',
      },
      success: {
        default: 'var(--ion-color-success)',
        shade: 'var(--ion-color-success-shade)',
        tint: 'var(--ion-color-success-tint)',
      },
      warning: {
        default: 'var(--ion-color-warning)',
        shade: 'var(--ion-color-warning-shade)',
        tint: 'var(--ion-color-warning-tint)',
      },
      danger: {
        default: 'var(--ion-color-danger)',
        shade: 'var(--ion-color-danger-shade)',
        tint: 'var(--ion-color-danger-tint)',
      },
      transparent: 'transparent',
      current: 'currentColor',

      black: colors.black,
      white: colors.white,
      gray: colors.coolGray,
      red: colors.red,
      yellow: colors.amber,
      green: colors.emerald,
      blue: colors.blue,
      indigo: colors.indigo,
      purple: colors.violet,
      pink: colors.pink,
      sky: colors.sky,
      olive: {
        '50': '#fbfbf4',
        '100': '#f9fad9',
        '200': '#f2f493',
        '300': '#e6e749',
        '400': '#c4cf15',
        '500': '#98bd1d',
        '600': '#6d9203',
        '700': '#537105',
        '800': '#3e5409',
        '900': '#30400b',
      },
      limegreen: {
        '50': '#f8fbf6',
        '100': '#f4fae5',
        '200': '#e4f4b5',
        '300': '#c9e977',
        '400': '#87d433',
        '500': '#49ba13',
        '600': '#319b0b',
        '700': '#2d7b0e',
        '800': '#265c12',
        '900': '#1f4713',
      },
      steel: {
        '50': '#f3fafc',
        '100': '#def7fb',
        '200': '#b3edf6',
        '300': '#7fddf3',
        '400': '#3bbfef',
        '500': '#179ae8',
        '600': '#1178d8',
        '700': '#145fb4',
        '800': '#144884',
        '900': '#123a65',
      },
      royalblue: {
        '50': '#f5fafd',
        '100': '#e6f7fc',
        '200': '#c2e7fa',
        '300': '#99d1fa',
        '400': '#61a8f9',
        '500': '#317cf8',
        '600': '#2257f1',
        '700': '#1f45d8',
        '800': '#1a35a6',
        '900': '#162b80',
      },
      step: {
        '50': 'var(--ion-color-step-50)',
        '100': 'var(--ion-color-step-100)',
        '150': 'var(--ion-color-step-150)',
        '200': 'var(--ion-color-step-200)',
        '250': 'var(--ion-color-step-250)',
        '300': 'var(--ion-color-step-300)',
        '350': 'var(--ion-color-step-350)',
        '400': 'var(--ion-color-step-400)',
        '450': 'var(--ion-color-step-450)',
        '500': 'var(--ion-color-step-500)',
        '550': 'var(--ion-color-step-550)',
        '600': 'var(--ion-color-step-600)',
        '650': 'var(--ion-color-step-650)',
        '700': 'var(--ion-color-step-700)',
        '750': 'var(--ion-color-step-750)',
        '800': 'var(--ion-color-step-800)',
        '850': 'var(--ion-color-step-850)',
        '900': 'var(--ion-color-step-900)',
        '950': 'var(--ion-color-step-950)',
      },
    },
    spacing: {
      px: '1px',
      0: '0px',
      0.5: '0.125rem',
      1: '0.25rem',
      1.5: '0.375rem',
      2: '0.5rem',
      2.5: '0.625rem',
      3: '0.75rem',
      3.5: '0.875rem',
      4: '1rem',
      5: '1.25rem',
      6: '1.5rem',
      7: '1.75rem',
      8: '2rem',
      9: '2.25rem',
      10: '2.5rem',
      11: '2.75rem',
      12: '3rem',
      14: '3.5rem',
      16: '4rem',
      20: '5rem',
      24: '6rem',
      28: '7rem',
      32: '8rem',
      36: '9rem',
      40: '10rem',
      44: '11rem',
      48: '12rem',
      52: '13rem',
      56: '14rem',
      60: '15rem',
      64: '16rem',
      72: '18rem',
      80: '20rem',
      96: '24rem',
      128: '32rem',
    },
    borderColor: (theme) => ({
      ...theme('colors'),
      DEFAULT: theme('colors.gray.200', 'currentColor'),
    }),
    borderOpacity: (theme) => theme('opacity'),
    borderRadius: {
      none: '0px',
      sm: '0.125rem',
      DEFAULT: '0.25rem',
      md: '0.375rem',
      lg: '0.5rem',
      xl: '0.75rem',
      '2xl': '1rem',
      '3xl': '1.5rem',
      full: '9999px',
    },
    borderWidth: {
      DEFAULT: '1px',
      0: '0px',
      2: '2px',
      4: '4px',
      8: '8px',
    },
    outline: {
      none: ['2px solid transparent', '2px'],
      white: ['2px dotted white', '2px'],
      black: ['2px dotted black', '2px'],
    },
    extend: {
      boxShadow: {
        'primary-sm': '0 1px 2px 0 rgba(var(--ion-color-primary-rgb), 0.05)',
        'primary-smooth': '0 1px 1px rgba(var(--ion-color-primary-rgb),0.035), 0 2px 2px rgba(var(--ion-color-primary-rgb),0.035), 0 4px 4px rgba(var(--ion-color-primary-rgb),0.035), 0 8px 8px rgba(var(--ion-color-primary-rgb),0.035);',
        'primary-md': '0 4px 6px -1px rgba(var(--ion-color-primary-rgb), 0.1), 0 2px 4px -1px rgba(var(--ion-color-primary-rgb), 0.06)',
        'primary-lg': '0 10px 15px -3px rgba(var(--ion-color-primary-rgb), 0.1), 0 4px 6px -2px rgba(var(--ion-color-primary-rgb), 0.05)',
        'primary-xl': '0 20px 25px -5px rgba(var(--ion-color-primary-rgb), 0.1), 0 10px 10px -5px rgba(var(--ion-color-primary-rgb), 0.04)',
        'primary-2xl': '0 25px 50px -12px rgba(var(--ion-color-primary-rgb), 0.25)',
        'primary-3xl': '0 35px 60px -15px rgba(var(--ion-color-primary-rgb), 0.3)',
        'primary-inner': 'inset 0 2px 4px 0 rgba(var(--ion-color-primary-rgb), 0.06)',
        none: 'none',
      },
    },
  },
  variants: {
    translate: ['responsive', 'hover', 'focus'],
    zIndex: ['responsive', 'focus-within', 'focus'],
    opacity: ['responsive', 'group-hover', 'focus-within', 'hover', 'focus', 'disabled'],
    backgroundOpacity: ['responsive', 'group-hover', 'focus-within', 'hover', 'focus'],
    backgroundColor: ['responsive', 'dark', 'group-hover', 'focus-within', 'hover', 'focus', 'disabled'],
    borderCollapse: ['responsive'],
    borderColor: ['responsive', 'dark', 'group-hover', 'focus-within', 'hover', 'focus'],
    borderOpacity: ['responsive', 'group-hover', 'focus-within', 'hover', 'focus'],
    borderRadius: ['responsive'],
    borderStyle: ['responsive'],
    borderWidth: ['responsive'],
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms')({
      strategy: 'class',
    }),
    require('@tailwindcss/aspect-ratio')
  ],
};
